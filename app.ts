import dotenv from 'dotenv';
import Server from './models/server';

// configuration dot.env
dotenv.config();

const server = new Server();

server.listen();
