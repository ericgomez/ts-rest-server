import { Request, response, Response } from 'express';
import User from '../models/user';

export const getUsers = async (req: Request, res: Response) => {
  // get users
  const users = await User.findAll();

  res.json(users);
};

export const getUser = async (req: Request, res: Response) => {
  const { id } = req.params;

  const user = await User.findByPk(id);

  if (user) {
    res.json(user);
  } else {
    res.status(404).json({
      msg: `User not exist with id ${id}`,
    });
  }
};

export const postUser = async (req: Request, res: Response) => {
  const { body } = req;

  try {
    const existEmail = await User.findOne({
      where: {
        email: body.email,
      },
    });

    if (existEmail) {
      return res.status(400).json({
        msg: `Email ${body.email} exist in db`,
      });
    }

    // Change by new instance of class abstract
    const user = User.build(body);
    await user.save();

    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: 'Internal Error',
    });
  }
};

export const putUser = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { body } = req;

  try {
    const user = await User.findByPk(id);

    if (!user) {
      return res.status(404).json({
        msg: `User not exist with id ${id}`,
      });
    }

    // const existEmail = await User.findOne({
    //   where: {
    //     email: body.email,
    //   },
    // });

    // if (existEmail) {
    //   return res.status(400).json({
    //     msg: `Email ${body.email} exist in db`,
    //   });
    // }

    // update in db
    await user!.update(body);
    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: 'Internal Error',
    });
  }
};

export const deleteUser = async (req: Request, res: Response) => {
  const { id } = req.params;

  const user = await User.findByPk(id);

  if (!user) {
    return res.status(404).json({
      msg: `User not exist with id ${id}`,
    });
  }

  // TODO: Deleted logic
  await user.update({ status: false });

  // TODO: Deleted physical
  // await user.destroy();

  res.json(user);
};
