import express, { Application } from 'express';
import cors from 'cors';

import userRoutes from '../routes/user';
import db from '../db/connection';

class Server {
  private app: Application;
  private port: string;
  private apiPaths = {
    users: '/api/users',
  };

  constructor() {
    this.app = express();
    this.port = process.env.PORT || '8000';

    // init methods
    this.dbConnection();
    this.middlewares();
    // Definition of routes
    this.routes();
  }

  //TODO: Connect database
  async dbConnection() {
    try {
      await db.authenticate();
      console.log('Database online');
    } catch (err: any) {
      throw new Error(err);
    }
  }

  middlewares() {
    // CORS
    this.app.use(cors());
    // read of the body
    this.app.use(express.json());
    // folder public
    this.app.use(express.static('public'));
  }

  routes() {
    this.app.use(this.apiPaths.users, userRoutes);
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log('Server is running in the port ' + this.port);
    });
  }
}

export default Server;
